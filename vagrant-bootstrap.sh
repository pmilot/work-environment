#!/usr/bin/env bash

# Tell apt-get that we're in a script
export DEBIAN_FRONTEND=noninteractive

# Symlink some interesting things from home
ln -s /vagrant/.bashrc /home/vagrant/.bash_aliases
ln -s /vagrant/.vim /home/vagrant/.vim
ln -s /vagrant/.vimrc /home/vagrant/.vimrc
ln -s /vagrant/.gitconfig /home/vagrant/.gitconfig

# Copy SSH config (this can't be a symlink sadly)
cp /vagrant/.ssh/config /home/vagrant/.ssh/config
chown vagrant:vagrant /home/vagrant/.ssh/config
chmod 600 /home/vagrant/.ssh/config

# Add docker APT repository
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sh -c "echo deb https://get.docker.com/ubuntu docker main > /etc/apt/sources.list.d/docker.list"

# Update packages
apt-get update

# The basics
apt-get install -y vim git build-essential python-pip cifs-utils

# Docker
apt-get install -y lxc-docker

# nginx
apt-get install -y nginx

# Fig
pip install fig

# Programming languages
apt-get install -y haskell-platform sbcl buildapp

# Documentation utilities
apt-get install -y python-sphinx texlive

# Add vagrant user to docker group
usermod -a -G docker vagrant

# Setup docs.vagrant vhost in nginx
mkdir -p /var/www/docs.vagrant
cp /vagrant/nginx/docs.vagrant /etc/nginx/sites-available/docs.vagrant
ln -s /etc/nginx/sites-available/docs.vagrant /etc/nginx/sites-enabled/docs.vagrant
service nginx restart

