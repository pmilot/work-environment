set hlsearch
set incsearch
set scrolloff=3 
set sidescrolloff=3
set undolevels=1000
set showmode

set nocompatible
set ff=unix
set encoding=utf-8
set fileencodings=
syntax enable
set t_Co=256
set backspace=indent,eol,start
set expandtab
set shiftwidth=2
set softtabstop=2
set autoindent
set wildignore+=*.class,*/var/*,*/target/*
set splitright
set hidden
set mouse=a
set laststatus=2
set fillchars+=vert:\ 
filetype plugin on

" Highlight characters beyond 80 columns
set colorcolumn=81

" Mapping to mkdir the path to the currently-opened buffer
nmap <Leader>md :!mkdir -p %:p:h<cr>

" Ctrl-P config
let g:ctrlp_max_files=0
let g:ctrlp_custom_ignore = {
\   'dir': '\v[\/]\.(git|hg|svn)$',
\   'file': '\v\.(exe|so|dll|a|lib|o|d|obj|exp)$',
\ }

" Syntastic config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" FSwitch configuration to switch between a Scala file and its Unit Test
" easily.
augroup scala
  au!
  au BufEnter *.scala let b:fswitchdst = 'scala'
  au BufEnter *.scala let b:fswitchfnames = '/$/Spec/'
  au BufEnter *.scala let b:fswitchlocs = 'reg:/main/test/'

  au BufEnter *Spec.scala let b:fswitchfnames = '/Spec$//'
  au BufEnter *Spec.scala let b:fswitchlocs = 'reg:/test/main/'
augroup END

" Mapping to facilitate using FSwitch
nmap <silent> <Leader>of :FSHere<cr>
nmap <silent> <Leader>ol :FSRight<cr>
nmap <silent> <Leader>oL :FSSplitRight<cr>
nmap <silent> <Leader>oh :FSLeft<cr>
nmap <silent> <Leader>oH :FSSplitLeft<cr>
nmap <silent> <Leader>ok :FSAbove<cr>
nmap <silent> <Leader>oK :FSSplitAbove<cr>
nmap <silent> <Leader>oj :FSBelow<cr>
nmap <silent> <Leader>oJ :FSSplitBelow<cr>

" Line-comments
nmap <silent> <Leader>cc :'a,'bs/^/\/\//g<cr>
nmap <silent> <Leader>cd :'a,'bs/^\/\///g<cr>

" NERDTree
nmap <C-n> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1

" YouCompleteMe
nmap <F12> :YcmCompleter GoTo<CR>

" C++ Development shortcuts
autocmd filetype cpp nnoremap <F9> :make! \| cw<CR>
autocmd filetype cpp nnoremap <F10> :make! all test CTEST_OUTPUT_ON_FAILURE=1 \| cw<CR>

" Rust Setup
let g:rust_clip_command = 'xclip -selection clipboard'
let g:rustfmt_autosave = 1
nmap <silent> <Leader>tt :RustTest!<cr>

" Common Lisp Setup
let g:slimv_swank_cmd = '! tmux new-window -d -n REPL-SBCL "sbcl --load ~/.vim/bundle/slimv.git/slime/start-swank.lisp"'

" Set syntax for nunjucks
autocmd BufNewFile,BufRead *.jinja2,*.j2,*.jinja,*.nunjucks,*.nunjs  set ft=jinja

