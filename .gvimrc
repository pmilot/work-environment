" gVim clean mode
set antialias
set guicursor+=a:blinkon0
set guioptions+=r
set guioptions-=l
set guioptions-=L
set guioptions-=T

set columns=120
set lines=50

colorscheme zenburn

if has("gui_win32")
  set guifont=Consolas:h12:cANSI:qDRAFT
else
  set guifont=Inconsolata\ 16
endif

" Configure fullscreen gVim on Windows
if has('libcall')
  let g:MyVimLib = $VIMRUNTIME.'/gvimfullscreen.dll'
  let g:VimMenuEnabled = 1
  function ToggleFullScreen()
    if g:VimMenuEnabled == 1
      let g:VimMenuEnabled = 0
      set go-=m
    else
      let g:VimMenuEnabled = 1
      set go+=m
    endif
    call libcallnr(g:MyVimLib, "ToggleFullScreen", 0)
  endfunction

  "F11 toggle fullscreen
  map <F11> <Esc>:call ToggleFullScreen()<CR>

  let g:VimAlpha = 240
  function! SetAlpha(alpha)
    let g:VimAlpha = g:VimAlpha + a:alpha
    if g:VimAlpha < 180
      let g:VimAlpha = 180
    endif
    if g:VimAlpha > 255
      let g:VimAlpha = 255
    endif
    call libcall(g:MyVimLib, 'SetAlpha', g:VimAlpha)
  endfunction

  "Shift+Y
  nmap <s-y> <Esc>:call SetAlpha(3)<CR>
  "Shift+T
  nmap <s-t> <Esc>:call SetAlpha(-3)<CR>

  let g:VimTopMost = 0
  function! SwitchVimTopMostMode()
    if g:VimTopMost == 0
      let g:VimTopMost = 1
    else
      let g:VimTopMost = 0
    endif
    call libcall(g:MyVimLib, 'EnableTopMost', g:VimTopMost)
  endfunction

  "Shift+R
  nmap <s-r> <Esc>:call SwitchVimTopMostMode()<CR>
endif

