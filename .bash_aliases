export SVN_EDITOR=vim
export EDITOR=vim
export TERMINAL=gnome-terminal

# Path
export PATH=$HOME/go/bin:$PATH
export PATH=$HOME/.cargo/bin:$PATH
export PATH=$HOME/.gem/ruby/2.7.0/bin:$PATH

# Mail
export MAIL=$HOME/.mail/philmilot-zoho-com/INBOX

alias phpxd="php -dzend_extension=xdebug.so"

# POWERLINE!
source .bash-powerline.sh

# Base16 Shell
BASE16_SHELL="$HOME/work-environment/scripts/base16-shell/base16-default.dark.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL

function tunnel()
{
  ssh -f -N "$@"
}

function g()
{
  grep -nr "$@" * | grep -v svn | grep -v templates_c | grep -nr "$@"
}

function gi()
{
  grep -nir "$@" * | grep -v svn | grep -v templates_c | grep -nir "$@"
}

function svnd() { svn diff $1 > f.diff ; vim f.diff ; rm f.diff ; }
function gitd() { git diff $1 > f.diff ; vim f.diff ; rm f.diff ; }

function parse_git_branch {

  ref=$(git symbolic-ref HEAD 2> /dev/null) || return

  echo " ("${ref#refs/heads/}")"

}

function deleteswp() {
  find . -name *.swp -exec rm -f {} \;
}

function runqueue() {
  php ~/$@/lib/heap-core/scripts/heap_qmgr.php -c 1000
}

if [ -S "$XDG_RUNTIME_DIR/ssh-agent.socket" ]; then
  export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi

if [ "$OS" == "Windows_NT" ]; then
  eval $(keychain --eval --agents ssh .ssh/id_rsa)
fi

