# This script is used to bootstrap a new VM.
# It assumes a brand-new Debian Jessie installation.
# If VBox Guest Additions CD is inserted, it will be detected and installed.
# Script must be run as root to work.

VBOXGA=/media/cdrom/VBoxLinuxAdditions.run
PMILOT=/home/pmilot

# Add third-party repositories
# FPComplete
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 575159689BEFB442
echo 'deb http://download.fpcomplete.com/debian jessie main' | tee /etc/apt/sources.list.d/fpco.list
# Sinew (Enpass.io)
echo "deb http://repo.sinew.in/ stable main" > /etc/apt/sources.list.d/enpass.list
wget -O - http://repo.sinew.in/keys/enpass-linux.key | apt-key add -

# Update packages
apt-get update

# Install packages
dpkg --add-architecture i386
apt-get update
apt-get install linux-headers-`uname -r`
cat packages | xargs apt-get install -y

# Set alias for nodejs so npm doesn't break
ln -s nodejs /usr/bin/node

# Set vim as default editor
update-alternatives --set editor /usr/bin/vim.basic

# Set urxvt as default X terminal emulator
update-alternatives --set x-terminal-emulator /usr/bin/gnome-terminal.wrapper

# Set uzbl as default X browser
update-alternatives --set x-www-browser /usr/bin/uzbl-browser

# Install VBox guest additions and setup symlinks for user
if [ -a $VBOXGA ]; then
  sh $VBOXGA
  usermod -a -G vboxsf pmilot
  ln -f -s /media/sf_Dropbox $PMILOT/dropbox
  chown pmilot:pmilot $PMILOT/dropbox
  ln -f -s /media/sf_OneDrive $PMILOT/onedrive
  chown pmilot:pmilot $PMILOT/onedrive
fi

# Install docker
echo deb https://apt.dockerproject.org/repo debian-jessie main > /etc/apt/sources.list.d/docker.list
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-get update
apt-get install docker-engine
usermod -aG docker pmilot

# Add pmilot to sudoers
usermod -aG sudo pmilot

# Install crontab
crontab pmilot crontab

# Install enpass symlink
ln -s /opt/Enpass/bin/runenpass.sh /usr/local/bin/enpass

